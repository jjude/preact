# 100 Preact + htm projects

I'm learning Preact with htm. This repository contains the code I use to learn.

## Usage
I am using the no-build approach, which means you should be able to clone the repository, open the html in the browser and it should work.

## Support
If you have any questions or clarifications, create an issue. Or reach out to me on twitter @jjude

## License
You are free to use all the code whatever way you want to use. It would be better if you attribute by linking to this repository. But it is not necessary.
